import React, { Component } from "react";
import Counter from "./Counter";
import CounterFunctionalComponent from "./CounterFunctionalComponent";
import "./App.css";
import FormikComponent from "./FormikComponent";

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mount: true,
      value: 1,
    };
    this.mountCounter = () => this.setState({ mount: true });
    this.unmountCounter = () => this.setState({ mount: false });
    this.incrementValue = () => this.setState({ value: this.state.value + 1 });
  }
  render() {
    return (
      <div className="container">
        {/* <div className="btns-container">
          <button onClick={this.incrementValue}>increment value</button>
          <button onClick={this.mountCounter} disabled={this.state.mount}>
            mount
          </button>

          <button onClick={this.unmountCounter} disabled={!this.state.mount}>
            unmount
          </button>
        </div>
        <div>{this.state.mount && <Counter value={this.state.value} />}</div>
        <div>
          {this.state.mount && <CounterFunctionalComponent value={999} />}
        </div> */}
        <FormikComponent />
      </div>
    );
  }
}

export default App;
