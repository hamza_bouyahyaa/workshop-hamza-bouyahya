import React, { Component } from "react";
import "./App.css";

export default class Counter extends Component {
  constructor(props) {
    console.log("constructor");
    super(props);
    this.state = { counter: 0 };
    this.increment = () => this.setState({ counter: this.state.counter + 1 });
    this.decrement = () => this.setState({ counter: this.state.counter - 1 });
  }
  componentDidMount() {
    console.log("componentDidMount");
    console.log("----------------------------------------");
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("component did update");
    console.log("----------------------------------------");
  }
  componentWillUnmount() {
    console.log("componentWillUnmount");
  }
  render() {
    console.log("Render");
    return (
      <div className="App">
        <div className="btns-container">
          <button onClick={this.increment}>Increment</button>
          <button onClick={this.decrement}>Decrement</button>
        </div>
        {/* ValueProps: {this.props.value} */}
        CounterClassComponent: {this.state.counter}
      </div>
    );
  }
}
