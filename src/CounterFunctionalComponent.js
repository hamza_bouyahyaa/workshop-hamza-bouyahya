import React, { useEffect, useState } from "react";

const CounterFunctionalComponent = ({ value }) => {
  const [counter, setCounter] = useState(0);
  useEffect(() => {
    console.log("UseEffect analogy of component Did Mount");
    return () => {
      console.log("UseEffect analogy of component Will Unmount");
    };
  }, []);
  useEffect(() => {
    console.log("UseEffect analogy of component Did Update");
  }, [value]);

  return (
    <div className="App">
      <div className="btns-container">
        <button onClick={() => setCounter(counter + 1)}>Increment</button>
        <button onClick={() => setCounter(counter - 1)}>Decrement</button>
      </div>
      CounterFunctionalComponent: {counter}
    </div>
  );
};

export default CounterFunctionalComponent;
